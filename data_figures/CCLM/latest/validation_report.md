# Validation report 

## General information

|||
|---|---|
|**Name:** | **`compare_to_era5`**|
|Created at:                    |`2023-02-09 13:12:10`|
|Created for output directory:  |`/silod8/karsten/work/IOW_ESM_3nm_ERA5/output/era5_reference_run_monthly/CCLM_Eurocordex`|
|Start date:                    |`19600101`|
|End date:                      |`20191231`|
|Author:                        |`karsten`|




### Experiment description

***

<h4 style="background-color: rgba(31, 119, 180, 0.5);"><b>coupled</b></h4>




This is an example for the coupled model.


    

### Performed tasks

***


#### **Two-dimensional seasonal means**

This task generates figures containing two-dimensional seasonal means $\langle \phi \rangle_S(x,y)$ for a season $s$ and a three-dimensional variable $\phi(x,y,t)$, i.e.$$ \langle \phi \rangle_S(x,y) = \frac{1}{N_S} \sum_{t\in S} \phi(x,y,t), $$where $t$ are all time steps that are contained in season $S$. The number of these time steps is given by $N_S$, where the considered time period is from `19600101` to `20191231`.


#### **Two-dimensional seasonal anomalies**

This task generates figures containing two-dimensional seasonal anomalies $\langle \Delta \phi \rangle_S(x,y)$ for a season $s$, a three-dimensional variable $\phi(x,y,t)$ and a three-dimensional reference field $\phi_{\mathrm{ref}}(x,y,t)$ i.e.$$ \langle \Delta \phi \rangle_S(x,y) = \frac{1}{N_S} \sum_{t\in S} \phi(x,y,t) - \phi_{\mathrm{ref}}(x,y,t) = \langle \phi \rangle_S(x,y) - \langle \phi_{\mathrm{ref}} \rangle_S(x,y) , $$where $t$ are all time steps that are contained in season $S$. The number of these time steps is given by $N_S$, where the considered time period is from `19600101` to `20191231`.


#### **Stations and Regions**

This task generates an image with the configured staitons and regions for which time series data is generated.For regions the time series is generated as the spatial mean over this region.Whereas for stations a remapping to the nearest neighboring grid cell is performed.


#### **Time series**

This task generates figures of temporal means according to the configured time-series operators, e.g. if you specfied `time_series_operators = ["-monmean"]`, a plot with monthly means is generated.Please refer to the `cdo` documentation for more information on these operators.If there are more than 100 samples in the time series and it is compared to reference data, a scatter plot is generated, where the $x$ and $y$ coordinates correspond to the reference samples and the model ones, respectively.


#### **Taylor Diagrams**

Taylor diagrams graphically indicate which of several model data represents best a given reference data.In order to quantify the degree of correspondence between the modeled and observed behavior in terms of three statistics:the Pearson correlation coefficient, the root-mean-square error (RMSE) error, and the standard deviation.Here both data, model and reference, consist of the same number of samples that correspond to a time series starting from `19600101` and ending at `20191231`.


#### **Cost functions**

The cost function $c$ as it is defined here, further summarizes the information given in a Taylor diagram.It measures the root means square error $\epsilon = \sqrt{\frac{1}{N}\sum_{t=t_1}^{t_{N}} (\phi(t)-\phi_{\mathrm{ref}}(t))^2}$ of the model data $\phi(t)$in units of the standard deviation $\sigma_{\mathrm{ref}}$ of reference data $\phi_{\mathrm{ref}}(t)$, i.e.$$ c = \epsilon / \sigma_{\mathrm{ref}}. $$Both data consist of $N$ samples corresponding to a time series starting from $t_1$ and ending at $t_N$.


#### **Vertical profiles**

This task generates vertical profiles of a four-dimensional field $\phi(x, y, z, t)$ at configured stations (using remapping to nearest neighbors) accompanied by performing the configured seasonal means.Vertical profiles that correspong to regions are created by an additional spatial mean over the particular region.




## Results



### Averaged 2 meter air temperature (T_2M_AV)   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    

#### **Description**

This parameter is the 2 meter air temperature that is averaged over output period, e.g. for daily output it is the daily average.



<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: rainbow, contour: True, delta_value: 2.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 298.15, min_value: 268.15, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_2m_temperature_*.nc`

---
##### reference-additional-operators
`-b F32`

---
##### reference-variable-name
`t2m`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.5, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 3.0, min_value: -3.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: T_2M_AV, vert_name: z, width: None`

---
##### other-models
`{}`

---
##### long-name
`averaged 2 meter air temperature`

---
##### description
`This parameter is the 2 meter air temperature that is averaged over output period, e.g. for daily output it is the daily average.`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/T_2M_AV.png">
    <figcaption align = "center"> <b> Fig. 1: </b> <b> Seasonal means for variable averaged 2 meter air temperature (T_2M_AV). </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/T_2M_AV.png">
    <figcaption align = "center"> <b> Fig. 2: </b> <b> Seasonal anomalies for variable averaged 2 meter air temperature (T_2M_AV). </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/T_2M_AV.png">
    <figcaption align = "center"> <b> Fig. 3: </b> <b> Stations and regions for variable averaged 2 meter air temperature (T_2M_AV). </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/T_2M_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 4: </b> <b>Time series for variable averaged 2 meter air temperature (T_2M_AV). </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/T_2M_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 5: </b> <b>Time series for variable averaged 2 meter air temperature (T_2M_AV). </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/T_2M_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 6: </b> <b> Taylor diagrams for variable averaged 2 meter air temperature (T_2M_AV). </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/T_2M_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 7: </b> <b> Taylor diagrams for variable averaged 2 meter air temperature (T_2M_AV). </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/T_2M_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 8: </b> <b> Cost functions $c$ for variable averaged 2 meter air temperature (T_2M_AV). </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/T_2M_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 9: </b> <b> Cost functions $c$ for variable averaged 2 meter air temperature (T_2M_AV). </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### DAY_PREC   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: YlGnBu, contour: True, delta_value: 1.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 6.0, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_total_precipitation_*.nc`

---
##### reference-additional-operators
`-b F32 -mulc,1000.0 -setattribute,tp@units="kg/m^2"`

---
##### reference-variable-name
`tp`

---
##### plot-config-anomaly
`color_map: BrBG, contour: True, delta_value: 0.2, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 1.0, min_value: -1.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: DAY_PREC, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/DAY_PREC.png">
    <figcaption align = "center"> <b> Fig. 10: </b> <b> Seasonal means for variable DAY_PREC. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/DAY_PREC.png">
    <figcaption align = "center"> <b> Fig. 11: </b> <b> Seasonal anomalies for variable DAY_PREC. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/DAY_PREC.png">
    <figcaption align = "center"> <b> Fig. 12: </b> <b> Stations and regions for variable DAY_PREC. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/DAY_PREC-stations.png">
    <figcaption align = "center"> <b> Fig. 13: </b> <b>Time series for variable DAY_PREC. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/DAY_PREC-regions.png">
    <figcaption align = "center"> <b> Fig. 14: </b> <b>Time series for variable DAY_PREC. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/DAY_PREC-stations.png">
    <figcaption align = "center"> <b> Fig. 15: </b> <b> Taylor diagrams for variable DAY_PREC. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/DAY_PREC-regions.png">
    <figcaption align = "center"> <b> Fig. 16: </b> <b> Taylor diagrams for variable DAY_PREC. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/DAY_PREC-stations.png">
    <figcaption align = "center"> <b> Fig. 17: </b> <b> Cost functions $c$ for variable DAY_PREC. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/DAY_PREC-regions.png">
    <figcaption align = "center"> <b> Fig. 18: </b> <b> Cost functions $c$ for variable DAY_PREC. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### ASWD_S   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'year': '', 'Jan': '1', 'Feb': '2', 'Mar': '3', 'Apr': '4', 'May': '5', 'Jun': '6', 'Jul': '7', 'Aug': '8', 'Sep': '9', 'Oct': '10', 'Nov': '11', 'Dec': '12'}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: inferno, contour: True, delta_value: 30.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 300.0, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_mean_surface_downward_short_wave_radiation_flux_*.nc`

---
##### reference-additional-operators
`-b F32`

---
##### reference-variable-name
`msdwswrf`

---
##### plot-config-anomaly
`color_map: seismic, contour: False, delta_value: 6.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 30.0, min_value: -30.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: ASWD_S, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/ASWD_S.png">
    <figcaption align = "center"> <b> Fig. 19: </b> <b> Seasonal means for variable ASWD_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/ASWD_S.png">
    <figcaption align = "center"> <b> Fig. 20: </b> <b> Seasonal anomalies for variable ASWD_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/ASWD_S.png">
    <figcaption align = "center"> <b> Fig. 21: </b> <b> Stations and regions for variable ASWD_S. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/ASWD_S-stations.png">
    <figcaption align = "center"> <b> Fig. 22: </b> <b>Time series for variable ASWD_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/ASWD_S-regions.png">
    <figcaption align = "center"> <b> Fig. 23: </b> <b>Time series for variable ASWD_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/ASWD_S-stations.png">
    <figcaption align = "center"> <b> Fig. 24: </b> <b> Taylor diagrams for variable ASWD_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/ASWD_S-regions.png">
    <figcaption align = "center"> <b> Fig. 25: </b> <b> Taylor diagrams for variable ASWD_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/ASWD_S-stations.png">
    <figcaption align = "center"> <b> Fig. 26: </b> <b> Cost functions $c$ for variable ASWD_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/ASWD_S-regions.png">
    <figcaption align = "center"> <b> Fig. 27: </b> <b> Cost functions $c$ for variable ASWD_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### ALWD_S   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: rainbow, contour: True, delta_value: 30.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 500.0, min_value: 200.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_mean_surface_downward_long_wave_radiation_flux_*.nc`

---
##### reference-additional-operators
`-b F32`

---
##### reference-variable-name
`msdwlwrf`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 10.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 40.0, min_value: -40.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: ALWD_S, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/ALWD_S.png">
    <figcaption align = "center"> <b> Fig. 28: </b> <b> Seasonal means for variable ALWD_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/ALWD_S.png">
    <figcaption align = "center"> <b> Fig. 29: </b> <b> Seasonal anomalies for variable ALWD_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/ALWD_S.png">
    <figcaption align = "center"> <b> Fig. 30: </b> <b> Stations and regions for variable ALWD_S. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/ALWD_S-stations.png">
    <figcaption align = "center"> <b> Fig. 31: </b> <b>Time series for variable ALWD_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/ALWD_S-regions.png">
    <figcaption align = "center"> <b> Fig. 32: </b> <b>Time series for variable ALWD_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/ALWD_S-stations.png">
    <figcaption align = "center"> <b> Fig. 33: </b> <b> Taylor diagrams for variable ALWD_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/ALWD_S-regions.png">
    <figcaption align = "center"> <b> Fig. 34: </b> <b> Taylor diagrams for variable ALWD_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/ALWD_S-stations.png">
    <figcaption align = "center"> <b> Fig. 35: </b> <b> Cost functions $c$ for variable ALWD_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/ALWD_S-regions.png">
    <figcaption align = "center"> <b> Fig. 36: </b> <b> Cost functions $c$ for variable ALWD_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### SPEED_10M_AV   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: terrain_r, contour: False, delta_value: 0.5, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 7.0, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_10m_wind_speed_*.nc`

---
##### reference-additional-operators
`-b F32`

---
##### reference-variable-name
`si10`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.5, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 2.0, min_value: -2.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: SPEED_10M_AV, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/SPEED_10M_AV.png">
    <figcaption align = "center"> <b> Fig. 37: </b> <b> Seasonal means for variable SPEED_10M_AV. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/SPEED_10M_AV.png">
    <figcaption align = "center"> <b> Fig. 38: </b> <b> Seasonal anomalies for variable SPEED_10M_AV. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/SPEED_10M_AV.png">
    <figcaption align = "center"> <b> Fig. 39: </b> <b> Stations and regions for variable SPEED_10M_AV. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/SPEED_10M_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 40: </b> <b>Time series for variable SPEED_10M_AV. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/SPEED_10M_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 41: </b> <b>Time series for variable SPEED_10M_AV. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/SPEED_10M_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 42: </b> <b> Taylor diagrams for variable SPEED_10M_AV. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/SPEED_10M_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 43: </b> <b> Taylor diagrams for variable SPEED_10M_AV. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/SPEED_10M_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 44: </b> <b> Cost functions $c$ for variable SPEED_10M_AV. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/SPEED_10M_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 45: </b> <b> Cost functions $c$ for variable SPEED_10M_AV. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### PMSL_AV   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: RdYlBu_r, contour: True, delta_value: 150.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 102200.0, min_value: 100200.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_mean_sea_level_pressure_*.nc`

---
##### reference-additional-operators
`-b F32`

---
##### reference-variable-name
`msl`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 50.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 300.0, min_value: -300.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: PMSL_AV, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/PMSL_AV.png">
    <figcaption align = "center"> <b> Fig. 46: </b> <b> Seasonal means for variable PMSL_AV. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/PMSL_AV.png">
    <figcaption align = "center"> <b> Fig. 47: </b> <b> Seasonal anomalies for variable PMSL_AV. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/PMSL_AV.png">
    <figcaption align = "center"> <b> Fig. 48: </b> <b> Stations and regions for variable PMSL_AV. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/PMSL_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 49: </b> <b>Time series for variable PMSL_AV. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/PMSL_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 50: </b> <b>Time series for variable PMSL_AV. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/PMSL_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 51: </b> <b> Taylor diagrams for variable PMSL_AV. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/PMSL_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 52: </b> <b> Taylor diagrams for variable PMSL_AV. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/PMSL_AV-stations.png">
    <figcaption align = "center"> <b> Fig. 53: </b> <b> Cost functions $c$ for variable PMSL_AV. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/PMSL_AV-regions.png">
    <figcaption align = "center"> <b> Fig. 54: </b> <b> Cost functions $c$ for variable PMSL_AV. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### AEVAP_S   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: BrBG_r, contour: True, delta_value: 0.3, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.0, min_value: -3.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_mean_evaporation_rate_*.nc`

---
##### reference-additional-operators
`-b F32 -mulc,86400.0 -setattribute,mer@units="kg/m^2"`

---
##### reference-variable-name
`mer`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.2, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 1.0, min_value: -1.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: AEVAP_S, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/AEVAP_S.png">
    <figcaption align = "center"> <b> Fig. 55: </b> <b> Seasonal means for variable AEVAP_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/AEVAP_S.png">
    <figcaption align = "center"> <b> Fig. 56: </b> <b> Seasonal anomalies for variable AEVAP_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/AEVAP_S.png">
    <figcaption align = "center"> <b> Fig. 57: </b> <b> Stations and regions for variable AEVAP_S. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/AEVAP_S-stations.png">
    <figcaption align = "center"> <b> Fig. 58: </b> <b>Time series for variable AEVAP_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/AEVAP_S-regions.png">
    <figcaption align = "center"> <b> Fig. 59: </b> <b>Time series for variable AEVAP_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/AEVAP_S-stations.png">
    <figcaption align = "center"> <b> Fig. 60: </b> <b> Taylor diagrams for variable AEVAP_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/AEVAP_S-regions.png">
    <figcaption align = "center"> <b> Fig. 61: </b> <b> Taylor diagrams for variable AEVAP_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/AEVAP_S-stations.png">
    <figcaption align = "center"> <b> Fig. 62: </b> <b> Cost functions $c$ for variable AEVAP_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/AEVAP_S-regions.png">
    <figcaption align = "center"> <b> Fig. 63: </b> <b> Cost functions $c$ for variable AEVAP_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### ALHFL_S   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: BrBG_r, contour: True, delta_value: 15.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.0, min_value: -150.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_mean_surface_latent_heat_flux*.nc`

---
##### reference-variable-name
`mslhf`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 2.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 20.0, min_value: -20.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: ALHFL_S, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/ALHFL_S.png">
    <figcaption align = "center"> <b> Fig. 64: </b> <b> Seasonal means for variable ALHFL_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/ALHFL_S.png">
    <figcaption align = "center"> <b> Fig. 65: </b> <b> Seasonal anomalies for variable ALHFL_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/ALHFL_S.png">
    <figcaption align = "center"> <b> Fig. 66: </b> <b> Stations and regions for variable ALHFL_S. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/ALHFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 67: </b> <b>Time series for variable ALHFL_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/ALHFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 68: </b> <b>Time series for variable ALHFL_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/ALHFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 69: </b> <b> Taylor diagrams for variable ALHFL_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/ALHFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 70: </b> <b> Taylor diagrams for variable ALHFL_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/ALHFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 71: </b> <b> Cost functions $c$ for variable ALHFL_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/ALHFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 72: </b> <b> Cost functions $c$ for variable ALHFL_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### ASHFL_S   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: Spectral, contour: True, delta_value: 10.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.0, min_value: -100.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_mean_surface_sensible_heat_flux*.nc`

---
##### reference-variable-name
`msshf`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 2.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 20.0, min_value: -20.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: ASHFL_S, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/ASHFL_S.png">
    <figcaption align = "center"> <b> Fig. 73: </b> <b> Seasonal means for variable ASHFL_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/ASHFL_S.png">
    <figcaption align = "center"> <b> Fig. 74: </b> <b> Seasonal anomalies for variable ASHFL_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/ASHFL_S.png">
    <figcaption align = "center"> <b> Fig. 75: </b> <b> Stations and regions for variable ASHFL_S. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/ASHFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 76: </b> <b>Time series for variable ASHFL_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/ASHFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 77: </b> <b>Time series for variable ASHFL_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/ASHFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 78: </b> <b> Taylor diagrams for variable ASHFL_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/ASHFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 79: </b> <b> Taylor diagrams for variable ASHFL_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/ASHFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 80: </b> <b> Cost functions $c$ for variable ASHFL_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/ASHFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 81: </b> <b> Cost functions $c$ for variable ASHFL_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### CLCT   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: Blues, contour: True, delta_value: 0.1, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 1.0, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_total_cloud_cover*.nc`

---
##### reference-variable-name
`tcc`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.05, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.3, min_value: -0.3, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: CLCT, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/CLCT.png">
    <figcaption align = "center"> <b> Fig. 82: </b> <b> Seasonal means for variable CLCT. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/CLCT.png">
    <figcaption align = "center"> <b> Fig. 83: </b> <b> Seasonal anomalies for variable CLCT. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/CLCT.png">
    <figcaption align = "center"> <b> Fig. 84: </b> <b> Stations and regions for variable CLCT. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/CLCT-stations.png">
    <figcaption align = "center"> <b> Fig. 85: </b> <b>Time series for variable CLCT. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/CLCT-regions.png">
    <figcaption align = "center"> <b> Fig. 86: </b> <b>Time series for variable CLCT. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/CLCT-stations.png">
    <figcaption align = "center"> <b> Fig. 87: </b> <b> Taylor diagrams for variable CLCT. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/CLCT-regions.png">
    <figcaption align = "center"> <b> Fig. 88: </b> <b> Taylor diagrams for variable CLCT. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/CLCT-stations.png">
    <figcaption align = "center"> <b> Fig. 89: </b> <b> Cost functions $c$ for variable CLCT. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/CLCT-regions.png">
    <figcaption align = "center"> <b> Fig. 90: </b> <b> Cost functions $c$ for variable CLCT. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### AUMFL_S   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: Spectral, contour: True, delta_value: 0.003, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.03, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_mean_eastward_turbulent_surface_stress_*.nc`

---
##### reference-variable-name
`metss`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.005, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.03, min_value: -0.03, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: AUMFL_S, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/AUMFL_S.png">
    <figcaption align = "center"> <b> Fig. 91: </b> <b> Seasonal means for variable AUMFL_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/AUMFL_S.png">
    <figcaption align = "center"> <b> Fig. 92: </b> <b> Seasonal anomalies for variable AUMFL_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/AUMFL_S.png">
    <figcaption align = "center"> <b> Fig. 93: </b> <b> Stations and regions for variable AUMFL_S. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/AUMFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 94: </b> <b>Time series for variable AUMFL_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/AUMFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 95: </b> <b>Time series for variable AUMFL_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/AUMFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 96: </b> <b> Taylor diagrams for variable AUMFL_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/AUMFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 97: </b> <b> Taylor diagrams for variable AUMFL_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/AUMFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 98: </b> <b> Cost functions $c$ for variable AUMFL_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/AUMFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 99: </b> <b> Cost functions $c$ for variable AUMFL_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### AVMFL_S   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: Spectral, contour: True, delta_value: 0.004, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.02, min_value: -0.02, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Eurocordex/reanalysis-era5-single-levels-monthly-means_mean_northward_turbulent_surface_stress_*.nc`

---
##### reference-variable-name
`mntss`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.005, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.03, min_value: -0.03, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: AVMFL_S, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/AVMFL_S.png">
    <figcaption align = "center"> <b> Fig. 100: </b> <b> Seasonal means for variable AVMFL_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/AVMFL_S.png">
    <figcaption align = "center"> <b> Fig. 101: </b> <b> Seasonal anomalies for variable AVMFL_S. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/AVMFL_S.png">
    <figcaption align = "center"> <b> Fig. 102: </b> <b> Stations and regions for variable AVMFL_S. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/AVMFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 103: </b> <b>Time series for variable AVMFL_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/AVMFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 104: </b> <b>Time series for variable AVMFL_S. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/AVMFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 105: </b> <b> Taylor diagrams for variable AVMFL_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/AVMFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 106: </b> <b> Taylor diagrams for variable AVMFL_S. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/AVMFL_S-stations.png">
    <figcaption align = "center"> <b> Fig. 107: </b> <b> Cost functions $c$ for variable AVMFL_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/AVMFL_S-regions.png">
    <figcaption align = "center"> <b> Fig. 108: </b> <b> Cost functions $c$ for variable AVMFL_S. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### RELHUM_2M   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: gist_earth_r, contour: True, delta_value: 10.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 100.0, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/RELHUM_2M.png">
    <figcaption align = "center"> <b> Fig. 109: </b> <b> Seasonal means for variable RELHUM_2M. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/RELHUM_2M.png">
    <figcaption align = "center"> <b> Fig. 110: </b> <b> Stations and regions for variable RELHUM_2M. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/RELHUM_2M-stations.png">
    <figcaption align = "center"> <b> Fig. 111: </b> <b>Time series for variable RELHUM_2M. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/RELHUM_2M-regions.png">
    <figcaption align = "center"> <b> Fig. 112: </b> <b>Time series for variable RELHUM_2M. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>



</details>



### T   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'mean': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'ROSTOCK-WARNEMUNDE': {'lat': '54.18', 'lon': '12.08'}, 'STOCKHOLM': {'lat': '59.35', 'lon': '18.05'}, 'TALLINN': {'lat': '59:23:53', 'lon': '24:36:10'}, 'VISBY': {'lat': '57:40:00', 'lon': '18:19:59'}, 'SUNDSVALL': {'lat': '62:24:36', 'lon': '17:16:12'}, 'LULEA': {'lat': '65:37:12', 'lon': '22:07:48'}, 'VAASA-PALOSAARI': {'lat': '63:06:00', 'lon': '21:36:00'}}`

---
##### regions
`{'VALID_DOMAIN': {'lat-min': '35.0', 'lat-max': '70.0', 'lon-min': '-10.0', 'lon-max': '35.0'}, 'NORTHERN_LANDS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_LANDS.nc'}, 'BALTIC_CATCHMENT': {'maskfile': '/silod8/karsten/masks/Eurocordex/BALTIC_CATCHMENT.nc'}, 'NORTHERN_WATERS': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTHERN_WATERS.nc'}, 'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'NORTH_SEA': {'maskfile': '/silod8/karsten/masks/Eurocordex/NORTH_SEA.nc'}}`

---
##### dimension
`4`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: ocean, contour: True, delta_value: None, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 298.15, min_value: 173.14999999999998, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/T.png">
    <figcaption align = "center"> <b> Fig. 113: </b> <b> Stations and regions for variable T. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Vertical profiles**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_vertical_profiles/results/compare_to_era5_era5_reference_run_monthly_CCLM_Eurocordex-19600101_20191231/compare_vertical_profiles.ipynb)



<figure>
<img src="./figures/compare_vertical_profiles/T-stations.png">
    <figcaption align = "center"> <b> Fig. 114: </b> <b> Vertical profiles for variable T. </b>Shaded areas depict the 95% confidence interval around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_vertical_profiles/T-regions.png">
    <figcaption align = "center"> <b> Fig. 115: </b> <b> Vertical profiles for variable T. </b>Shaded areas depict the 95% confidence interval around the mean values. </figcaption>
</figure>  



</details>



</details>

