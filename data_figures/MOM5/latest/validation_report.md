# Validation report 

## General information

|||
|---|---|
|**Name:** | **`compare_to_era5`**|
|Created at:                    |`2023-02-09 11:04:38`|
|Created for output directory:  |`/silod8/karsten/work/IOW_ESM_3nm_ERA5/output/era5_reference_run_monthly/MOM5_Baltic`|
|Start date:                    |`19600101`|
|End date:                      |`20191231`|
|Author:                        |`karsten`|



    
### Report description


This is an example for the post-processing configuration.


    

### Experiment description

***

<h4 style="background-color: rgba(31, 119, 180, 0.5);"><b>model1</b></h4>



### Performed tasks

***


#### **Two-dimensional seasonal means**

This task generates figures containing two-dimensional seasonal means $\langle \phi \rangle_S(x,y)$ for a season $s$ and a three-dimensional variable $\phi(x,y,t)$, i.e.$$ \langle \phi \rangle_S(x,y) = \frac{1}{N_S} \sum_{t\in S} \phi(x,y,t), $$where $t$ are all time steps that are contained in season $S$. The number of these time steps is given by $N_S$, where the considered time period is from `19600101` to `20191231`.


#### **Two-dimensional seasonal anomalies**

This task generates figures containing two-dimensional seasonal anomalies $\langle \Delta \phi \rangle_S(x,y)$ for a season $s$, a three-dimensional variable $\phi(x,y,t)$ and a three-dimensional reference field $\phi_{\mathrm{ref}}(x,y,t)$ i.e.$$ \langle \Delta \phi \rangle_S(x,y) = \frac{1}{N_S} \sum_{t\in S} \phi(x,y,t) - \phi_{\mathrm{ref}}(x,y,t) = \langle \phi \rangle_S(x,y) - \langle \phi_{\mathrm{ref}} \rangle_S(x,y) , $$where $t$ are all time steps that are contained in season $S$. The number of these time steps is given by $N_S$, where the considered time period is from `19600101` to `20191231`.


#### **Stations and Regions**

This task generates an image with the configured staitons and regions for which time series data is generated.For regions the time series is generated as the spatial mean over this region.Whereas for stations a remapping to the nearest neighboring grid cell is performed.


#### **Time series**

This task generates figures of temporal means according to the configured time-series operators, e.g. if you specfied `time_series_operators = ["-monmean"]`, a plot with monthly means is generated.Please refer to the `cdo` documentation for more information on these operators.If there are more than 100 samples in the time series and it is compared to reference data, a scatter plot is generated, where the $x$ and $y$ coordinates correspond to the reference samples and the model ones, respectively.


#### **Taylor Diagrams**

Taylor diagrams graphically indicate which of several model data represents best a given reference data.In order to quantify the degree of correspondence between the modeled and observed behavior in terms of three statistics:the Pearson correlation coefficient, the root-mean-square error (RMSE) error, and the standard deviation.Here both data, model and reference, consist of the same number of samples that correspond to a time series starting from `19600101` and ending at `20191231`.


#### **Cost functions**

The cost function $c$ as it is defined here, further summarizes the information given in a Taylor diagram.It measures the root means square error $\epsilon = \sqrt{\frac{1}{N}\sum_{t=t_1}^{t_{N}} (\phi(t)-\phi_{\mathrm{ref}}(t))^2}$ of the model data $\phi(t)$in units of the standard deviation $\sigma_{\mathrm{ref}}$ of reference data $\phi_{\mathrm{ref}}(t)$, i.e.$$ c = \epsilon / \sigma_{\mathrm{ref}}. $$Both data consist of $N$ samples corresponding to a time series starting from $t_1$ and ending at $t_N$.


#### **Vertical profiles**

This task generates vertical profiles of a four-dimensional field $\phi(x, y, z, t)$ at configured stations (using remapping to nearest neighbors) accompanied by performing the configured seasonal means.Vertical profiles that correspong to regions are created by an additional spatial mean over the particular region.




## Results



### Sea surface temperature (SST)   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    

#### **Description**

This parameter is the temperature of sea water near the surface measured in degrees Celsius. The corresponding model output variable is called SST.

#### **Reference description**

Reference is taken from https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels-monthly-means. Search for "Sea surface temperature"



<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`{'min_value': 0.0, 'max_value': 15.0, 'delta_value': 1.0, 'contour': True, 'color_map': 'rainbow'}`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_sea_surface_temperature*.nc`

---
##### reference-variable-name
`sst`

---
##### reference-additional-operators
`-b F32 -setattribute,sst@units=Celsius -subc,273.15`

---
##### reference-description
`Reference is taken from https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels-monthly-means. Search for "Sea surface temperature"`

---
##### plot-config-anomaly
`{'min_value': -5.0, 'max_value': 5.0, 'delta_value': 1.0, 'contour': True, 'color_map': 'seismic'}`

---
##### other-models
`{}`

---
##### long-name
`sea surface temperature`

---
##### description
`This parameter is the temperature of sea water near the surface measured in degrees Celsius. The corresponding model output variable is called SST.`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/SST.png">
    <figcaption align = "center"> <b> Fig. 1: </b> <b> Seasonal means for variable sea surface temperature (SST). </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/SST.png">
    <figcaption align = "center"> <b> Fig. 2: </b> <b> Seasonal anomalies for variable sea surface temperature (SST). </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/SST.png">
    <figcaption align = "center"> <b> Fig. 3: </b> <b> Stations and regions for variable sea surface temperature (SST). </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/SST-stations.png">
    <figcaption align = "center"> <b> Fig. 4: </b> <b>Time series for variable sea surface temperature (SST). </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/SST-regions.png">
    <figcaption align = "center"> <b> Fig. 5: </b> <b>Time series for variable sea surface temperature (SST). </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/SST-stations.png">
    <figcaption align = "center"> <b> Fig. 6: </b> <b> Taylor diagrams for variable sea surface temperature (SST). </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/SST-regions.png">
    <figcaption align = "center"> <b> Fig. 7: </b> <b> Taylor diagrams for variable sea surface temperature (SST). </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/SST-stations.png">
    <figcaption align = "center"> <b> Fig. 8: </b> <b> Cost functions $c$ for variable sea surface temperature (SST). </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/SST-regions.png">
    <figcaption align = "center"> <b> Fig. 9: </b> <b> Cost functions $c$ for variable sea surface temperature (SST). </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### Fraction of ice (FI)   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    

#### **Description**

This parameter is the ice coverage of the grid cells, where 0 for no ice and 1 means that the cell completely covered. The corresponding model output has been calculated from a sum of sea ice concentrations (model variable `CN`) from all ice layers categorized by thickness.



<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`{'min_value': 0.0, 'max_value': 1.0, 'delta_value': 0.1, 'contour': True, 'color_map': 'Blues_r'}`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_sea_ice_cover*.nc`

---
##### reference-variable-name
`siconc`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.1, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.6, min_value: -0.6, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---
##### long-name
`fraction of ice`

---
##### description
`This parameter is the ice coverage of the grid cells, where 0 for no ice and 1 means that the cell completely covered. The corresponding model output has been calculated from a sum of sea ice concentrations (model variable `CN`) from all ice layers categorized by thickness.`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/FI.png">
    <figcaption align = "center"> <b> Fig. 10: </b> <b> Seasonal means for variable fraction of ice (FI). </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/FI.png">
    <figcaption align = "center"> <b> Fig. 11: </b> <b> Seasonal anomalies for variable fraction of ice (FI). </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/FI.png">
    <figcaption align = "center"> <b> Fig. 12: </b> <b> Stations and regions for variable fraction of ice (FI). </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/FI-stations.png">
    <figcaption align = "center"> <b> Fig. 13: </b> <b>Time series for variable fraction of ice (FI). </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/FI-regions.png">
    <figcaption align = "center"> <b> Fig. 14: </b> <b>Time series for variable fraction of ice (FI). </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/FI-stations.png">
    <figcaption align = "center"> <b> Fig. 15: </b> <b> Taylor diagrams for variable fraction of ice (FI). </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/FI-regions.png">
    <figcaption align = "center"> <b> Fig. 16: </b> <b> Taylor diagrams for variable fraction of ice (FI). </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/FI-stations.png">
    <figcaption align = "center"> <b> Fig. 17: </b> <b> Cost functions $c$ for variable fraction of ice (FI). </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/FI-regions.png">
    <figcaption align = "center"> <b> Fig. 18: </b> <b> Cost functions $c$ for variable fraction of ice (FI). </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### Salinity (salt)   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    

#### **Description**

This parameter is the four-dimensional ($x,y,z,t$) practical salinity field.



<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### dimension
`4`

---
##### plot-config
`color_map: ocean, contour: True, delta_value: None, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 24.0, min_value: 0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### BED-reference-file-pattern
`/silod8/karsten/obs/BEDValidationData_1/Monthly/*SALIN*.dat`

---
##### other-models
`{}`

---
##### long-name
`salinity`

---
##### description
`This parameter is the four-dimensional ($x,y,z,t$) practical salinity field.`

---



</details>

    
#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/salt.png">
    <figcaption align = "center"> <b> Fig. 19: </b> <b> Stations and regions for variable salinity (salt). </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Vertical profiles**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_vertical_profiles/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_vertical_profiles.ipynb)



<figure>
<img src="./figures/compare_vertical_profiles/salt-stations.png">
    <figcaption align = "center"> <b> Fig. 20: </b> <b> Vertical profiles for variable salinity (salt). </b>Shaded areas depict the 95% confidence interval around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_vertical_profiles/salt-regions.png">
    <figcaption align = "center"> <b> Fig. 21: </b> <b> Vertical profiles for variable salinity (salt). </b>Shaded areas depict the 95% confidence interval around the mean values. </figcaption>
</figure>  



</details>



</details>



### Temperature (temp)   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    

#### **Description**

This parameter is the four-dimensional ($x,y,z,t$) conservative temperature field.



<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### dimension
`4`

---
##### plot-config
`{'min_value': 0, 'max_value': 20.0}`

---
##### BED-reference-file-pattern
`/silod8/karsten/obs/BEDValidationData_1/Monthly/*TEMP*.dat`

---
##### other-models
`{}`

---
##### long-name
`Temperature`

---
##### description
`This parameter is the four-dimensional ($x,y,z,t$) conservative temperature field.`

---



</details>

    
#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/temp.png">
    <figcaption align = "center"> <b> Fig. 22: </b> <b> Stations and regions for variable Temperature (temp). </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Vertical profiles**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_vertical_profiles/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_vertical_profiles.ipynb)



<figure>
<img src="./figures/compare_vertical_profiles/temp-stations.png">
    <figcaption align = "center"> <b> Fig. 23: </b> <b> Vertical profiles for variable Temperature (temp). </b>Shaded areas depict the 95% confidence interval around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_vertical_profiles/temp-regions.png">
    <figcaption align = "center"> <b> Fig. 24: </b> <b> Vertical profiles for variable Temperature (temp). </b>Shaded areas depict the 95% confidence interval around the mean values. </figcaption>
</figure>  



</details>



</details>



### Evaporation (EVAP)   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    

#### **Description**

This parameter is the evaporation from the ocean at the surface to the atmosphere, i.e. it is measured in positive upward direction.



<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: BrBG_r, contour: True, delta_value: 5e-06, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 5e-05, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_mean_evaporation_rate*.nc`

---
##### reference-variable-name
`mer`

---
##### reference-additional-operators
`-b F32 -mulc,-1.0`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 5e-06, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 2e-05, min_value: -2e-05, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---
##### long-name
`Evaporation`

---
##### description
`This parameter is the evaporation from the ocean at the surface to the atmosphere, i.e. it is measured in positive upward direction.`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/EVAP.png">
    <figcaption align = "center"> <b> Fig. 25: </b> <b> Seasonal means for variable Evaporation (EVAP). </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/EVAP.png">
    <figcaption align = "center"> <b> Fig. 26: </b> <b> Seasonal anomalies for variable Evaporation (EVAP). </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/EVAP.png">
    <figcaption align = "center"> <b> Fig. 27: </b> <b> Stations and regions for variable Evaporation (EVAP). </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/EVAP-stations.png">
    <figcaption align = "center"> <b> Fig. 28: </b> <b>Time series for variable Evaporation (EVAP). </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/EVAP-regions.png">
    <figcaption align = "center"> <b> Fig. 29: </b> <b>Time series for variable Evaporation (EVAP). </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/EVAP-stations.png">
    <figcaption align = "center"> <b> Fig. 30: </b> <b> Taylor diagrams for variable Evaporation (EVAP). </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/EVAP-regions.png">
    <figcaption align = "center"> <b> Fig. 31: </b> <b> Taylor diagrams for variable Evaporation (EVAP). </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/EVAP-stations.png">
    <figcaption align = "center"> <b> Fig. 32: </b> <b> Cost functions $c$ for variable Evaporation (EVAP). </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/EVAP-regions.png">
    <figcaption align = "center"> <b> Fig. 33: </b> <b> Cost functions $c$ for variable Evaporation (EVAP). </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### tau_x   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: Spectral, contour: True, delta_value: 0.003, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.03, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_mean_eastward_turbulent_surface_stress_*.nc`

---
##### reference-variable-name
`metss`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.005, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.03, min_value: -0.03, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/tau_x.png">
    <figcaption align = "center"> <b> Fig. 34: </b> <b> Seasonal means for variable tau_x. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/tau_x.png">
    <figcaption align = "center"> <b> Fig. 35: </b> <b> Seasonal anomalies for variable tau_x. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/tau_x.png">
    <figcaption align = "center"> <b> Fig. 36: </b> <b> Stations and regions for variable tau_x. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/tau_x-stations.png">
    <figcaption align = "center"> <b> Fig. 37: </b> <b>Time series for variable tau_x. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/tau_x-regions.png">
    <figcaption align = "center"> <b> Fig. 38: </b> <b>Time series for variable tau_x. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/tau_x-stations.png">
    <figcaption align = "center"> <b> Fig. 39: </b> <b> Taylor diagrams for variable tau_x. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/tau_x-regions.png">
    <figcaption align = "center"> <b> Fig. 40: </b> <b> Taylor diagrams for variable tau_x. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/tau_x-stations.png">
    <figcaption align = "center"> <b> Fig. 41: </b> <b> Cost functions $c$ for variable tau_x. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/tau_x-regions.png">
    <figcaption align = "center"> <b> Fig. 42: </b> <b> Cost functions $c$ for variable tau_x. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### tau_y   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: Spectral, contour: True, delta_value: 0.004, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.02, min_value: -0.02, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_mean_northward_turbulent_surface_stress_*.nc`

---
##### reference-variable-name
`mntss`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 0.005, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 0.03, min_value: -0.03, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/tau_y.png">
    <figcaption align = "center"> <b> Fig. 43: </b> <b> Seasonal means for variable tau_y. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/tau_y.png">
    <figcaption align = "center"> <b> Fig. 44: </b> <b> Seasonal anomalies for variable tau_y. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/tau_y.png">
    <figcaption align = "center"> <b> Fig. 45: </b> <b> Stations and regions for variable tau_y. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/tau_y-stations.png">
    <figcaption align = "center"> <b> Fig. 46: </b> <b>Time series for variable tau_y. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/tau_y-regions.png">
    <figcaption align = "center"> <b> Fig. 47: </b> <b>Time series for variable tau_y. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/tau_y-stations.png">
    <figcaption align = "center"> <b> Fig. 48: </b> <b> Taylor diagrams for variable tau_y. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/tau_y-regions.png">
    <figcaption align = "center"> <b> Fig. 49: </b> <b> Taylor diagrams for variable tau_y. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/tau_y-stations.png">
    <figcaption align = "center"> <b> Fig. 50: </b> <b> Cost functions $c$ for variable tau_y. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/tau_y-regions.png">
    <figcaption align = "center"> <b> Fig. 51: </b> <b> Cost functions $c$ for variable tau_y. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### swdn   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'year': '', 'Jan': '1', 'Feb': '2', 'Mar': '3', 'Apr': '4', 'May': '5', 'Jun': '6', 'Jul': '7', 'Aug': '8', 'Sep': '9', 'Oct': '10', 'Nov': '11', 'Dec': '12'}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: magma, contour: True, delta_value: 20.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 250.0, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_mean_surface_downward_short_wave_radiation_flux_*.nc`

---
##### reference-variable-name
`msdwswrf`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: False, delta_value: 6.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 30.0, min_value: -30.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/swdn.png">
    <figcaption align = "center"> <b> Fig. 52: </b> <b> Seasonal means for variable swdn. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/swdn.png">
    <figcaption align = "center"> <b> Fig. 53: </b> <b> Seasonal anomalies for variable swdn. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/swdn.png">
    <figcaption align = "center"> <b> Fig. 54: </b> <b> Stations and regions for variable swdn. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/swdn-stations.png">
    <figcaption align = "center"> <b> Fig. 55: </b> <b>Time series for variable swdn. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/swdn-regions.png">
    <figcaption align = "center"> <b> Fig. 56: </b> <b>Time series for variable swdn. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/swdn-stations.png">
    <figcaption align = "center"> <b> Fig. 57: </b> <b> Taylor diagrams for variable swdn. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/swdn-regions.png">
    <figcaption align = "center"> <b> Fig. 58: </b> <b> Taylor diagrams for variable swdn. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/swdn-stations.png">
    <figcaption align = "center"> <b> Fig. 59: </b> <b> Cost functions $c$ for variable swdn. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/swdn-regions.png">
    <figcaption align = "center"> <b> Fig. 60: </b> <b> Cost functions $c$ for variable swdn. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### lwdn   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: magma, contour: True, delta_value: 10.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 350.0, min_value: 200.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_mean_surface_downward_long_wave_radiation_flux_*.nc`

---
##### reference-variable-name
`msdwlwrf`

---
##### reference-additional-operators
`-b F32`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 2.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 10.0, min_value: -10.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/lwdn.png">
    <figcaption align = "center"> <b> Fig. 61: </b> <b> Seasonal means for variable lwdn. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/lwdn.png">
    <figcaption align = "center"> <b> Fig. 62: </b> <b> Seasonal anomalies for variable lwdn. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/lwdn.png">
    <figcaption align = "center"> <b> Fig. 63: </b> <b> Stations and regions for variable lwdn. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/lwdn-stations.png">
    <figcaption align = "center"> <b> Fig. 64: </b> <b>Time series for variable lwdn. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/lwdn-regions.png">
    <figcaption align = "center"> <b> Fig. 65: </b> <b>Time series for variable lwdn. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/lwdn-stations.png">
    <figcaption align = "center"> <b> Fig. 66: </b> <b> Taylor diagrams for variable lwdn. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/lwdn-regions.png">
    <figcaption align = "center"> <b> Fig. 67: </b> <b> Taylor diagrams for variable lwdn. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/lwdn-stations.png">
    <figcaption align = "center"> <b> Fig. 68: </b> <b> Cost functions $c$ for variable lwdn. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/lwdn-regions.png">
    <figcaption align = "center"> <b> Fig. 69: </b> <b> Cost functions $c$ for variable lwdn. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### LH   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: BrBG_r, contour: True, delta_value: 15.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 150.0, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_mean_surface_latent_heat_flux*.nc`

---
##### reference-variable-name
`mslhf`

---
##### reference-additional-operators
`-b F32 -mulc,-1.0`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 6.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 30.0, min_value: -30.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/LH.png">
    <figcaption align = "center"> <b> Fig. 70: </b> <b> Seasonal means for variable LH. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/LH.png">
    <figcaption align = "center"> <b> Fig. 71: </b> <b> Seasonal anomalies for variable LH. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/LH.png">
    <figcaption align = "center"> <b> Fig. 72: </b> <b> Stations and regions for variable LH. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/LH-stations.png">
    <figcaption align = "center"> <b> Fig. 73: </b> <b>Time series for variable LH. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/LH-regions.png">
    <figcaption align = "center"> <b> Fig. 74: </b> <b>Time series for variable LH. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/LH-stations.png">
    <figcaption align = "center"> <b> Fig. 75: </b> <b> Taylor diagrams for variable LH. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/LH-regions.png">
    <figcaption align = "center"> <b> Fig. 76: </b> <b> Taylor diagrams for variable LH. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/LH-stations.png">
    <figcaption align = "center"> <b> Fig. 77: </b> <b> Cost functions $c$ for variable LH. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/LH-regions.png">
    <figcaption align = "center"> <b> Fig. 78: </b> <b> Cost functions $c$ for variable LH. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>



### SH   

<hr style="border:2px solid gray">

<details>
<summary><b><i>Analysis</i></b></summary>

    





<details>
<summary><i>Postprocess settings</i></summary>

[**Go to settings ->**](../../../global_settings.py)

    
##### seasons
`{'MAM': '3,4,5', 'JJA': '6,7,8', 'SON': '9,10,11', 'DJF': '12,1,2', 'year': ''}`

---
##### percentiles
`[]`

---
##### stations
`{'BY5': {'lat': '55.25', 'lon': '15.98', 'alternative-names': ['BornholmdeepBY5']}, 'F9': {'lat': '64.71', 'lon': '22.07', 'alternative-names': ['BothnianBayF9']}, 'SR5': {'lat': '61.08', 'lon': '19.58', 'alternative-names': ['BothnianSeaSR5']}, 'BY31': {'lat': '58.58', 'lon': '18.23', 'alternative-names': ['LandsortDeepBY31']}, 'BY15': {'lat': '57.3333', 'lon': '20.05', 'alternative-names': ['GotlanddeepBY15']}, 'LL7': {'lat': '59.8465', 'lon': '24.8378', 'alternative-names': ['GulfFinlandLL7']}}`

---
##### regions
`{'BALTIC_SEA': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_SEA.nc'}, 'BOTHNIAN_GULF': {'maskfile': '/silod8/karsten/masks/Baltic/BOTHNIAN_GULF.nc'}, 'BALTIC_PROPER': {'maskfile': '/silod8/karsten/masks/Baltic/BALTIC_PROPER.nc'}, 'BELTS': {'maskfile': '/silod8/karsten/masks/Baltic/BELTS.nc'}, 'RIGA_FINLAND': {'maskfile': '/silod8/karsten/masks/Baltic/RIGA_FINLAND.nc'}, 'test': {'lat-min': '55.0', 'lat-max': '56.0', 'lon-min': '19.0', 'lon-max': '20.0'}}`

---
##### time-series-operators
`['-monmean', '-yearmean', '-ymonmean']`

---
##### plot-config
`color_map: Spectral, contour: True, delta_value: 5.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 50.0, min_value: 0.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### reference-file-pattern
`/silod8/karsten/era5/Baltic/reanalysis-era5-single-levels-monthly-means_mean_surface_sensible_heat_flux*.nc`

---
##### reference-variable-name
`msshf`

---
##### reference-additional-operators
`-b F32 -mulc,-1.0`

---
##### plot-config-anomaly
`color_map: seismic, contour: True, delta_value: 4.0, file: None, height: None, lat_name: lat, linestyle: None, lon_name: lon, max_value: 20.0, min_value: -20.0, path: None, std_deviation: True, symmetric: False, task_name: None, time_name: time, title: None, transform_variable: None, trend: False, variable: None, vert_name: z, width: None`

---
##### other-models
`{}`

---



</details>

    
#### **Two-dimensional seasonal means**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_means/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_means.ipynb)



<figure>
<img src="./figures/compare_2D_means/SH.png">
    <figcaption align = "center"> <b> Fig. 79: </b> <b> Seasonal means for variable SH. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Two-dimensional seasonal anomalies**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_2D_anomalies/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_2D_anomalies.ipynb)



<figure>
<img src="./figures/compare_2D_anomalies/SH.png">
    <figcaption align = "center"> <b> Fig. 80: </b> <b> Seasonal anomalies for variable SH. </b>The rows correspond to different models whereas the columns reflect the various seaons that are considered.The $x$ and $y$ axis measure the longitudes and latitudes, respectively. </figcaption>
</figure>  



</details>


#### **Stations and Regions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../draw_stations_and_regions/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/draw_stations_and_regions.ipynb)



<figure>
<img src="./figures/draw_stations_and_regions/SH.png">
    <figcaption align = "center"> <b> Fig. 81: </b> <b> Stations and regions for variable SH. </b>Colored areas depict the different regions. The dots are located at the station's coordinates. </figcaption>
</figure>  



</details>


#### **Time series**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../compare_time_series/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/compare_time_series.ipynb)



<figure>
<img src="./figures/compare_time_series/SH-stations.png">
    <figcaption align = "center"> <b> Fig. 82: </b> <b>Time series for variable SH. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



<figure>
<img src="./figures/compare_time_series/SH-regions.png">
    <figcaption align = "center"> <b> Fig. 83: </b> <b>Time series for variable SH. </b>Shaded areas depict the $\pm 2 \sigma$ vicinity (approximately the 95% confidence interval) around the mean values. </figcaption>
</figure>  



</details>


#### **Taylor Diagrams**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../create_taylor_diagrams/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/create_taylor_diagrams.ipynb)



<figure>
<img src="./figures/create_taylor_diagrams/SH-stations.png">
    <figcaption align = "center"> <b> Fig. 84: </b> <b> Taylor diagrams for variable SH. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



<figure>
<img src="./figures/create_taylor_diagrams/SH-regions.png">
    <figcaption align = "center"> <b> Fig. 85: </b> <b> Taylor diagrams for variable SH. </b>Colored stars stand for the model result and the black circle is the reference.The standard deviation of the data is measured on the radial axis whereas the correaltion to the reference is given by the angle;depicted is the arcus cosine of the angle.The colormap refers to the root means square error of the model data with respect to the reference data.The rows correspond to the different regions and stations whereas the columns are related to the different kind of time series. </figcaption>
</figure>  



</details>


#### **Cost functions**

***

<details>
<summary><b><i>Figures</b></i></summary>

[**Go to notebook ->**](../../../get_cost_function/results/compare_to_era5_era5_reference_run_monthly_MOM5_Baltic-19600101_20191231/get_cost_function.ipynb)



<figure>
<img src="./figures/get_cost_function/SH-stations.png">
    <figcaption align = "center"> <b> Fig. 86: </b> <b> Cost functions $c$ for variable SH. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



<figure>
<img src="./figures/get_cost_function/SH-regions.png">
    <figcaption align = "center"> <b> Fig. 87: </b> <b> Cost functions $c$ for variable SH. </b>The colors refer to the magnitude of the cost function: green means very good ($ 0 \leq c < 1 $),yellow stands for satisfactory ($ 1 < c < 2 $) and red shows bad quality ($ c \geq 2 $).**Bold** numbers correspond to the best performing model, whereas _italic_ number refer to the worst performing model for that particular station/region and kind of time series.The rows correspond to the different regions and stations whereas the columns are related to the different temporal means and models. </figcaption>
</figure>  



</details>



</details>

