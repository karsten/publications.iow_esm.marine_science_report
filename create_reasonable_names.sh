awk '
BEGIN{
	open=false
	author=""
    entries = 0
}
{   
    if(NF==0){
        next
    }
    a = $1
    if(a~"@"){
       c = 0
       open = true
    }
    if(a=="}" && open==true){
        if(author ~ /^[a-zA-Z]+$/ && year ~ /^[0-9]+$/){
            key = author year
        }
        else{
            key = original_key
        }        
        double_counter = 1
        for(i=0; i< entries; i++){
            if(key~keys[i]){
               double_counter++
            }
        }
        if(double_counter > 1){
            key = key"-"double_counter
        }

        keys[entries++] = key
        print l[0]key","
        for(i=1; i<c; i++){			                            
           print l[i]
        }

        print "}\n"
        open = false
    } 
    if(open==true){
        if(c==0){
            split($0,b,"{")
            l[c++]=b[1]"{"
            split(b[2],b,",")
            original_key=b[1]
            author=""
            year=""
        }
        else{   
            if($1=="author"){
                split($3,b,",")
                split(b[1],b,"{")
                author=b[2]
            }
            if($1=="year"){
                split($3,b,"{")
                split(b[2],b,"}")
                year=b[1]
            }

            l[c++]=$0
       }
   }

}' all_endnote.bib > all.bib

if [ -f "lib.bib" ]; then
    cat lib.bib >> all.bib
fi
